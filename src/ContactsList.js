import escapeStringRegexp from 'escape-string-regexp';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import sortBy from 'sort-by';
import {Link} from 'react-router-dom';

export class ContactsList extends Component {
    static propTypes = {
        contacts: PropTypes.array.isRequired,
        onDeleteContact: PropTypes.func.isRequired
    };

    state = {
        query: ''
    };

    changeQuery = query => {
        this.setState({query: query.trim()});
    };

    render() {
        const {contacts, onDeleteContact} = this.props;
        const {query} = this.state;

        let filteredContacts;
        if (query) {
            const match = new RegExp(escapeStringRegexp(query), 'i');
            filteredContacts = contacts.filter(contact => match.test(contact.name));
        } else {
            filteredContacts = contacts;
        }

        filteredContacts.sort(sortBy('name'));

        return (<div>
            <div className='list-contacts-top'>
                <input className="search-contacts"
                       value={query}
                       onChange={e => this.changeQuery(e.target.value)}
                       placeholder="Search contants"/>
                <Link className='add-contact' to="/create" />
            </div>
            <div className='list-contacts'>
                <ol className='contact-list'>
                    {filteredContacts.map(contact => (
                            <li key={contact.id} className='contact-list-item'>
                                <div className='contact-avatar' style={{
                                    backgroundImage: `url(${contact.avatarURL})`
                                }}/>
                                <div className='contact-details'>
                                    <p>{contact.name}</p>
                                    <p>{contact.email}</p>
                                </div>
                                <div className='contact-remove' onClick={() => onDeleteContact(contact)}>Remove</div>
                            </li>
                        )
                    )}
                </ol>
            </div>
        </div>);
    }
}