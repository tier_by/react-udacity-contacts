import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {ContactsList} from './ContactsList';
import CreateContact from './CreateContact';
import * as ContactApi from './utils/ContactsAPI';

class App extends Component {
    state = {
        contacts: []
    };

    componentDidMount() {
        ContactApi.getAll().then(contacts => {
            this.setState({contacts: contacts});
        });
    }

    createContact = contact => {
        ContactApi.create(contact).then(contact => {
            this.setState(state => ({
                    contacts: state.contacts.concat(contact)
                }));
        });
    };

    removeContact = contact => {
        this.setState(state => {
            return {
                contacts: state.contacts.filter(c => c.id !== contact.id)
            };
        });
        ContactApi.remove(contact);
    };

    render() {
        return (
            <div>
                <Route exact path="/" render={() => (
                    <ContactsList
                        contacts={this.state.contacts}
                        onDeleteContact={this.removeContact}
                    />
                )}/>
                <Route path='/create' render={({history}) => (
                    <CreateContact onCreateContact={contact => {
                        this.createContact(contact);
                        history.push('/');
                    }}/>
                )}/>
            </div>
        );
    }
}

export default App;
